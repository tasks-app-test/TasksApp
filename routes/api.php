<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => ['cors', 'json.response']], function () {

    // Public Routes

    // Auth
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);

    // Categories
    Route::get('categories', [CategoryController::class, 'index']);

    // Protected Routes
    Route::middleware('auth:api')->group(function () {
        Route::post('logout', [AuthController::class, 'logout'])->name('logout.api');

        // Tasks
        Route::get('tasks', [TaskController::class, 'index']);
        Route::post('tasks/create', [TaskController::class, 'store']);
        Route::patch('tasks/{id}/toggle', [TaskController::class, 'toggleIsEnded'])->whereNumber('id');
        Route::delete('tasks/{id}/destroy', [TaskController::class, 'destroy'])->whereNumber('id');
        Route::get('tasks/{id}', [TaskController::class, 'getById'])->whereNumber('id');
    })->name('public');
});
