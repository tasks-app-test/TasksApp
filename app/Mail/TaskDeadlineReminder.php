<?php

namespace App\Mail;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TaskDeadlineReminder extends Mailable
{
    use Queueable, SerializesModels;

    // Public properties to hold data for the email
    public $task; // You can pass the task data to the email template

    /**
     * Create a new message instance.
     *
     * @param  Task  $task
     * @return void
     */
    public function __construct($task)
    {
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Customize the email subject
        $subject = 'Task Deadline Reminder';

        return $this->subject($subject)
            ->view('emails.task-deadline-reminder');
    }
}
