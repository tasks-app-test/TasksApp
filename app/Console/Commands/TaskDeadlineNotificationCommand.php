<?php

namespace App\Console\Commands;

use App\Models\Task;
use App\Notifications\TaskDeadlineNotification;
use Illuminate\Console\Command;

class TaskDeadlineNotificationCommand extends Command
{
    protected $signature = 'app:task-deadline-notification-command';
    protected $description = 'Checks for exceeded deadlines and notifies users';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tasks = Task::where('deadline', '<', now())->where('is_ended', false)
            ->get();

        foreach ($tasks as $task) {
            // Queue the notification
            $task->user->notify(new TaskDeadlineNotification($task));
            $task->is_ended = true;
        }

        $this->info('Task deadline notifications sent.');
    }
}
