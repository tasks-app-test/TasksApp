<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Models\Subtask;
use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $user = auth('api')->user();
        return $user->tasks()->with(['subtasks', 'categories'])->get();
    }
    public function getById(int $id)
    {
        $user =
            auth('api')->user();
        $task = Task::with(['subtasks', 'categories'])->find($id);

        if (empty($task)) {
            return response()->json(['error' => 'not found'], 404);
        }

        if ($task->user_id != $user->id) {
            return response()->json(['error' => 'unauthorized'], 403);
        }

        return $task;
    }
    public function store(TaskRequest $request)
    {
        $validatedData = $request->validated();

        $user =
            auth('api')->user();

        // Create a new task, add subtasks, and associate it with the user 
        $task = new Task($validatedData);
        $user->tasks()->save($task);

        // Associate categories with the task
        $task->categories()->attach($validatedData['categories']);

        foreach ($validatedData['subtasks'] as $subtaskText) {
            $subtask = new Subtask(['description' => $subtaskText]);
            $task->subtasks()->save($subtask);
        }

        return response()->json(['messages' => 'success'], 200);
    }

    public function toggleIsEnded(int $id)
    {
        $user =
            auth('api')->user();
        $task = Task::find($id);

        if (empty($task)) {
            return response()->json(['error' => 'not found'], 404);
        }

        if ($task->user_id != $user->id) {
            return response()->json(['error' => 'unauthorized'], 403);
        }

        $task->is_ended = !$task->is_ended;
        $task->save();
        return response()->json(['messages' => 'success'], 200);
    }

    public function destroy(int $id)
    {
        $user =
            auth('api')->user();
        $task = Task::find($id);

        if (empty($task)) {
            return response()->json(['error' => 'not found'], 404);
        }

        if ($task->user_id != $user->id) {
            return response()->json(['error' => 'unauthorized'], 403);
        }

        $task->delete();
        return response()->json(['messages' => 'success'], 200);
    }
}
