<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'deadline' => 'required|date',
            'subtasks' => 'array',
            'subtasks.*' => 'string',
            'categories' => 'required|array',
            'categories.*' => 'numeric|exists:categories,id',
            'is_ended' => 'boolean',
        ];
    }
}
