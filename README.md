## TasksAppBackend

This is a private repo containing the backend of BeeOrder's Recruitment Test.

## Usage

- Run the following command: `npm install`

- Make sure to have "tasks-app" MySQL database or configure your own connection info in the .env file.

- Run the following command: `php artisan migrate`

- Run the following command: `php artisan db:seed`

- Run the following command: `php artisan serve`
