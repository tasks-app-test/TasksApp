<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        if (empty(User::where('email', 'user@mail.com')->first())) {
            $user = new User(['name' => 'Moutaz', 'email' => 'user@mail.com', 'password' => '123456789']);
            $user->save();

            $task1 = [
                'description' => 'Go to the supermarket',
                'deadline' => Carbon::parse('2023-09-25')->toDateString(),
                'is_ended' => true,
            ];

            $task2 = [
                'description' => 'Go to the job interview',
                'deadline' => Carbon::parse('2023-09-30')->toDateString(),
                'is_ended' => false,
            ];

            $task1 = $user->tasks()->create($task1);
            $task2 = $user->tasks()->create($task2);

            $task1->categories()->attach([3]);
            $task2->categories()->attach([1, 2]);

            $task2->subtasks()->createMany([
                ['description' => 'Get ready.'],
                ['description' => 'Go to the interview.'],
                ['description' => 'Get the job.']
            ]);
        }
    }
}
