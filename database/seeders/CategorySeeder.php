<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            ['name' => 'Important', 'color' => '#ff0e0e'],
            ['name' => 'Job', 'color' => '#288BA8'],
            ['name' => 'Home', 'color' => '#E389B9'],
            ['name' => 'Health', 'color' => '#FFCE30'],
        ];

        foreach ($categories as $category) {
            Category::firstOrCreate($category);
        }
    }
}
